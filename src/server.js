import express from "express";
import bodyParser from "body-parser";
import connectFlash from "connect-flash";
import cookieParser from "cookie-parser";
import passport from "passport";
import configViewEngine from "./config/viewEngine";
import configSession from "./config/session";
import initAllWebRoutes from "./routes/web";

let app = express();

// config express cookie
app.use(cookieParser("secret"));

// show flash message
app.use(connectFlash());

// config body-parser to post data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// config app session
configSession(app);

// config passport middleware
app.use(passport.initialize());
app.use(passport.session());

// config view engine
configViewEngine(app);

// init all web routes
initAllWebRoutes(app);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`App is running at the port ${PORT}`);
});
