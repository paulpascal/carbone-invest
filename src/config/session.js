require("dotenv").config();

let Sequelize = require("sequelize");
let session = require("express-session");

// initalize sequelize with session store
let SequelizeStore = require("connect-session-sequelize")(session.Store);

// connect to database
let db = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    logging: false,
    dialect: "postgres",
    storage: "./session.postgres",
  }
);

let sessionStore = new SequelizeStore({
  db: db,
});

let configSession = (app) => {
  app.use(
    session({
      key: "express.sid",
      secret: "secret",
      store: sessionStore,
      resave: true, // we support the touch method so per the express-session docs this should be set to false
      saveUninitialized: false,
      cookie: {
        httpOnly: false,
        secure: false,
        maxAge: 24 * 60 * 60 * 1000 * 1020, // 1 day
      },
    })
  );
};

// create the table in the db
sessionStore.sync();

module.exports = configSession;
