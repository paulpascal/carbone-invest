const ROLES = {
  superAdmin: "ROLE_SUPERADMIN",
  admin: "ROLE_ADMIN",
  partner: "ROLE_PARTNER",
  investor: "ROLE_INVESTOR",
};

module.exports = {
  ROLES: ROLES,
};
