const express = require("express");
const passport = require("passport");
const multer = require('multer')
const path = require('path');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    console.log('REQ', req.files, __dirname);
      cb(null, path.join(__dirname+'/../public/uploads'));
  },
  // By default, multer removes file extensions so let's add them back
  filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});
const upload = multer({storage: storage})

const homePageController = require("../controllers/homepageController");
const adminController = require("../controllers/adminController");
const initPassportLocal = require("../controllers/passport/passportLocal");
const authController = require("../controllers/authController");
const authValidator = require("../validation/authValidation");

/**
 * init passport local
 */
initPassportLocal();

/**
 * init all web routes here
 */

let router = express.Router();

let initAllWebRoutes = (app) => {
  // homepage
  router.get("/", homePageController.getHomePage);
  router.get("/invest", homePageController.getInvestPage);
  router.get("/trading-camp", homePageController.getTradingCampPage);
  router.get("/company_detail", homePageController.getCompanyDetailPage);

  // Auth
  router.get(
    "/login",
    authController.checkLoggedOut,
    homePageController.getLoginPage
  );
  router.get("/signup", homePageController.getSignupPage);
  router.get("/logout", authController.logout);
  router.post(
    "/handleLogin",
    passport.authenticate("local", {
      successRedirect: "/admin",
      failureRedirect: "/login",
      successFlash: true,
      failureFlash: true,
    })
  );
  router.post(
    "/handleSignup",
    authValidator.validateRegiser,
    homePageController.handleSignup
  );

  // admin
  router.get(
    "/admin",
    authController.checkLoggedIn,
    adminController.getAdminDashboard
  );

  // Partner
  router.get(
    "/admin/partners/add",
    authController.checkLoggedIn,
    adminController.addPartner
  );
  router.post(
    "/admin/partners/save",
    adminController.savePartner
  );
  router.get(
    "/admin/partners/:id/",
    authController.checkLoggedIn,
    adminController.getPartner
  );
  router.get(
    "/admin/partners",
    authController.checkLoggedIn,
    adminController.getPartnerList
  );

  router.post(
    "/admin/partners/update/:id",
    authController.checkLoggedIn,
    adminController.updatePartner
  );
  router.get(
    "/admin/partners/edit/:id",
    authController.checkLoggedIn,
    adminController.editPartner
  );

  router.get(
    "/admin/partners/delete/:id",
    authController.checkLoggedIn,
    adminController.deletePartner
  );

   // Plan
   router.get(
    "/admin/investment_plans/add",
    authController.checkLoggedIn,
    adminController.addPlan
  );
  router.post(
    "/admin/investment_plans/save",
    authController.checkLoggedIn,
    adminController.savePlan
  );
  router.get(
    "/admin/investment_plans/:id/",
    authController.checkLoggedIn,
    adminController.getPlan
  );
  router.get(
    "/admin/investment_plans",
    authController.checkLoggedIn,
    adminController.getPlanList
  );

  router.post(
    "/admin/investment_plans/update/:id",
    authController.checkLoggedIn,
    adminController.updatePlan
  );
  router.get(
    "/admin/investment_plans/edit/:id",
    authController.checkLoggedIn,
    adminController.editPlan
  );

  router.get(
    "/admin/investment_plans/delete/:id",
    authController.checkLoggedIn,
    adminController.deletePlan
  );


  // Demands
  router.get(
    "/admin/demands/show/:id",
    authController.checkLoggedIn,
    adminController.getDemand
  );
  router.get(
    "/admin/demands/status/:status",
    authController.checkLoggedIn,
    adminController.getDemandList
  );
  router.get(
    "/admin/demands",
    authController.checkLoggedIn,
    adminController.getDemandList
  );
  const cpUpload2 = upload.fields(
    [
      { name: 'contractFile', maxCount: 1 },
    ]
  );
  router.post(
    "/admin/demands/:id/validate",
    authController.checkLoggedIn, cpUpload2,
    adminController.validateDemand
  );
  // Process
  router.get(
    "/admin/process/:id",
    authController.checkLoggedIn,
    adminController.getProcess
  );
  router.get(
    "/admin/process",
    authController.checkLoggedIn,
    adminController.getProcessList
  );
  router.post(
    "/admin/process/launch",
    authController.checkLoggedIn,
    adminController.launchProcess
  );
  router.post(
    "/admin/process/update/:id",
    authController.checkLoggedIn,
    adminController.updateProcess
  );
  // Investment
  router.get(
    "/admin/investors",
    authController.checkLoggedIn,
    adminController.getInvestor
  );
  router.get(
    "/admin/investments/add",
    authController.checkLoggedIn,
    adminController.addInvestment
  );
  router.get(
    "/admin/investments/:id",
    authController.checkLoggedIn,
    adminController.getInvestment
  );
  router.get(
    "/admin/investments",
    authController.checkLoggedIn,
    adminController.getInvestmentList
  );
  const cpUpload = upload.fields(
    [
      { name: 'idFile', maxCount: 1 },
      { name: 'secondaryFile', maxCount: 1 }
    ]
  );
  router.post(
    "/admin/investments/save",
    authController.checkLoggedIn,
    cpUpload,
    adminController.saveInvestment
  );
  
  // User

  router.get(
    "/admin/users/add",
    authController.checkLoggedIn,
    adminController.addUser
  );
  router.get(
    "/admin/users",
    authController.checkLoggedIn,
    adminController.getUserList
  );

  router.post(
    "/admin/users/save",
    authController.checkLoggedIn,
    adminController.saveUser
  );
  router.post(
    "/admin/users/edit/:id",
    authController.checkLoggedIn,
    adminController.editUser
  );
  router.get(
    "/admin/users/delete/:id",
    authController.checkLoggedIn,
    adminController.deleteUser
  );
  router.get(
    "/admin/users/show/:id",
    authController.checkLoggedIn,
    adminController.showUser
  );

  return app.use(router);
};

module.exports = initAllWebRoutes;
