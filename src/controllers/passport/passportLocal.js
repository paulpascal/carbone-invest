const passport = require("passport");
const passportLocal = require("passport-local");
const authService = require("../../services/authService");

let LocalStrategy = passportLocal.Strategy;

let initPassportLocal = () => {
  passport.use(
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true,
      },
      async (req, email, password, done) => {
        try {
          let user = await authService.findUserByEmail(email);

          if (!user) {
            return done(
              null,
              false,
              req.flash(
                "errors",
                `Aucun compte associé à cette adresse email: '${email}'.`
              )
            );
          }

          await authService.comparePassword(password, user);
          return done(null, user, null);
        } catch (err) {
          console.log("CATCH: ", err);

          setTimeout(() => {
            return done(null, false, req.flash("errors", err));
          }, 2000);
        }
      }
    )
  );
};

passport.serializeUser((user, done) => {
  return done(null, user.id);
});

passport.deserializeUser((id, done) => {
  return authService
    .findUserById(id)
    .then((user) => {
      return done(null, user);
    })
    .catch((error) => {
      return done(error, null);
    });
});

module.exports = initPassportLocal;
