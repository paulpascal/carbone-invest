const moment = require("moment");
const { validationResult } = require("express-validator");

const userService = require("../services/userService");
const partnerService = require("../services/partnerService");
const demandService = require("../services/demandService");
const investmentPlanService = require("../services/investmentPlanService");
const attachmentService = require("../services/attachmentService");
const demand = require("../models/demand");

let getAdminDashboard = async (req, res) => {
  let user = req.user;
  
  if (user.isInvestor()) {
    let demands = await demandService.getInvestorDemands(user.id);
    return res.render("admin/main.ejs", { user: user,
      infos: req.flash("infos"),
      errors: req.flash("errors"),
      demands: demands, });
  }

  return res.render("admin/main.ejs", { user: user, infos: req.flash("infos"),
  errors: req.flash("errors"), });
};

// Partner
let getProcess = (req, res) => {
  let user = req.user;
  return res.render("admin/partner/listPartner.ejs", { user: user });
};
let getProcessList = (req, res) => {
  let user = req.user;
  return res.render("admin/partner/listPartner.ejs", { user: user });
};
let launchProcess = (req, res) => {
  let user = req.user;
  return res.render("admin/partner/addPartner.ejs", { user: user });
};
let updateProcess = (req, res) => {
  let user = req.user;
  return res.render("admin/partner/showPartner.ejs", { user: user });
};

// Demand
let getDemand = async (req, res) => {
  let user = req.user;
  let demand = await demandService.getDemand(req.params.id);
  console.log(demand)
  return res.render("admin/folder/showDemand.ejs", {
    user: user,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
    demand: demand,
  });
};
let getDemandList = async (req, res) => {
  let user = req.user;
  let status = req.params.status;
  
  let demands;

  if (user.isPartner()) {
    demands = await demandService.getPartnerDemands(status != null ? status == 'ok': null);

  } else if (user.isAdmin() || user.isSuperAdmin()) {
    demands = await demandService.getAll(status != null ? status == 'ok': null);
  }
  console.log('DEMAND STATUS: ', status != null ? status == 'ok': null)
  console.table(demands)

  return res.render("admin/folder/listDemand.ejs", {
    user: user,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
    demands: demands,
  });
};
let validateDemand = async (req, res) => {
  let user = req.user;
  try {
    let contractFile;
    
    if (!req.files['contractFile']) {
      req.flash("errors", "Le contrat n'a pas été uploadé.");
      let demand = await demandService.getDemand(req.params.id);
      return res.render("admin/folder/showDemand.ejs", {
        user: user,
        infos: req.flash("infos"),
        errors: req.flash("errors"),
        demand: demand,
      });
    } else {
      contractFile = await attachmentService.createNewAttachment({
        url: req.files.contractFile[0].filename,
        createdAt: Date.now(),
        updatedAt: Date.now(),
      })

      let demandForm = {
        state: true,
        updatedAt: Date.now()
      };
      let demand = await demandService.updateDemand(req.params.id, demandForm);
      await demand.setContract(contractFile)
  
      req.flash("infos", "La demande a été validé avec succès.");
      return res.redirect('/admin/demands/show/'  + demand.id);
    }
    
  } catch (err) {
    console.log(err);
    req.flash("errors", err);
  }
  
};

// Partner
let getPartner = (req, res) => {
  let user = req.user;
  return res.render("admin/partner/listPartner.ejs", { user: user });
};
let getPartnerList = async (req, res) => {
  let user = req.user;
  let partners = await partnerService.getAll();
  return res.render("admin/partner/listPartner.ejs", {
    user: user,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
    partners: partners,
  });
};
let addPartner = async (req, res) => {
  let user = req.user;
  let types = await partnerService.getAllTypes();
  // old input data
  return res.render("admin/partner/addPartner.ejs", {
    user: user,
    errors: req.flash("errors"),
    types: types,
  });
};
let savePartner = async (req, res) => {
  try {
    // create a new user
    let partner = {
      name: req.body.name,
      phone: req.body.phone,
      email: req.body.email,
      address: req.body.address,
      description: req.body.description,
      address: req.body.address,
      partnerTypeId: req.body.partnerTypeId,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    };
    console.log(partner);
    await partnerService.createNewPartner(partner);
    req.flash("infos", "Le partenaire a été créer avec succès.");
  } catch (err) {
    req.flash("errors", err);
  }
  return res.redirect("/admin/partners");
};

let updatePartner = async (req, res) => {
  try {
    // id
    let id = req.params.id;

    // create a new user
    let partner = {
      name: req.body.name,
      phone: req.body.phone,
      email: req.body.email,
      address: req.body.address,
      description: req.body.description,
      address: req.body.address,
      partnerTypeId: req.body.partnerTypeId,
      updatedAt: Date.now(),
    };
    await partnerService.updatePartner(id, partner);
    req.flash("infos", "Le partenaire a été modifié avec succès.");
    return res.redirect("/admin/partners");
  } catch (err) {
    req.flash("errors", err);
    return res.redirect("/admin/partners/add");
  }
};
let editPartner = async (req, res) => {
  let id = req.params.id;
  let user = req.user;
  let types = await partnerService.getAllTypes();
  let partner = await partnerService.getPartner(id);
  console.log(partner);

  return res.render("admin/partner/editPartner.ejs", {
    user: user,
    partner: partner,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
    types: types,
  });
};

let deletePartner = async (req, res) => {
  try {
    let id = req.params.id;
    await partnerService.deletePartner(id);
    req.flash("infos", "Partenaire supprimé avec succès");
  } catch (e) {
    console.log(e);
    req.flash("errors", e);
  }
  return res.redirect("/admin/partners");
};

// Investment Plans
let getPlan = (req, res) => {
  let user = req.user;
  return res.render("admin/investment_plan/listPlan.ejs", { user: user });
};
let getPlanList = async (req, res) => {
  let user = req.user;
  let plans = await investmentPlanService.getAll();
  return res.render("admin/investment_plan/listPlan.ejs", {
    user: user,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
    plans: plans,
  });
};
let addPlan = async (req, res) => {
  let user = req.user;
  // old input data
  return res.render("admin/investment_plan/addPlan.ejs", {
    user: user,
    errors: req.flash("errors"),
  });
};
let savePlan = async (req, res) => {
  try {
    let user = req.user;
    let plan = {
      title: req.body.title,
      minAmount: req.body.minAmount,
      maxAmount: req.body.maxAmount != '' ? req.body.maxAmount: null,
      startAt: req.body.startAt,
      endAt: req.body.endAt,
      partnerId: user.partnerId ? user.partnerId: 3,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    };
    await investmentPlanService.createNewPlan(plan);
    req.flash("infos", "Le plan a été créer avec succès.");
  } catch (err) {
    console.log(err);
    req.flash("errors", err);
  }
  return res.redirect("/admin/investment_plans");
};
let updatePlan = async (req, res) => {
  try {
    // id
    let id = req.params.id;

    let user = req.user;
    let plan = {
      title: req.body.title,
      minAmount: req.body.minAmount,
      maxAmount: req.body.maxAmount,
      startAt: req.body.startAt,
      endAt: req.body.endAt,
      partnerId: user.partnerId,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    };
    await investmentPlanService.updatePlan(id, plan);
    req.flash("infos", "Le plan a été modifié avec succès.");
    return res.redirect("/admin/investment_plans");
  } catch (err) {
    req.flash("errors", err);
    return res.redirect("/admin/investment_plans/add");
  }
};
let editPlan = async (req, res) => {
  let id = req.params.id;
  let user = req.user;
  

  return res.render("admin/investment_plans/editPlan.ejs", {
    user: user,
    plan: plan,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
  });
};
let deletePlan = async (req, res) => {
  try {
    let id = req.params.id;
    await investmentPlanService.deletePlan(id);
    req.flash("infos", "Plan supprimé avec succès");
  } catch (e) {
    console.log(e);
    req.flash("errors", e);
  }
  return res.redirect("/admin/investment_plans");
};

// User
let getUser = (req, res) => {
  let user = req.user;
  return res.render("admin/user/listUser.ejs", {
    user: user,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
  });
};
let getUserList = async (req, res) => {
  let user = req.user;
  let users = await userService.getUsers();
  return res.render("admin/user/listUser.ejs", {
    user: user,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
    users: users,
  });
};
let getUserListByPartner = (req, res) => {
  let user = req.user;
  return res.render("admin/user/listUser.ejs", {
    user: user,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
  });
};
let addUser = async (req, res) => {
  let user = req.user;
  let partners = await partnerService.getAll();

  // old input data
  let form = {
    firstName: req.body.firstName,
    lastName: req.body.firstName,
    email: req.body.email,
    password: req.body.password,
    confirmPassword: req.body.confirmPassword,
    address: "",
    role: req.body.role,
    partnerId: req.body.partnerId,
    phone: "",
  };
  return res.render("admin/user/addUser.ejs", {
    user: user,
    partners: partners,
    errors: req.flash("errors"),
    form: form,
  });
};

let saveUser = async (req, res) => {
  let user = req.user;
  let partners = await partnerService.getAll();

  try {
    // validate input fields
    let errorsArr = [];
    let validationError = validationResult(req);

    if (!validationError.isEmpty()) {
      let errors = Object.values(validationError.mapped());
      errors.forEach((item) => {
        errorsArr.push(item.msg);
      });
      req.flash("errors", errorsArr);

      return res.render("admin/user/addUser.ejs", {
        user: user,
        partners: partners,
        errors: req.flash("errors"),
        form: form,
      });
    }

    // create a new user
    let userData = {
      firstName: req.body.lastName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
      partnerId:
        req.body.partnerId == "null" ? null : parseInt(req.body.partnerId),
      confirmPassword: req.body.confirmPassword,
      address: "",
      phone: "",
      createdAt: Date.now(),
      updatedAt: Date.now(),
    };
    console.log(userData);
    await userService.createNewUser(userData);
    req.flash("infos", "Le compte a été bien créé avec succès..");
    return res.redirect("/admin/users");
  } catch (error) {
    console.log(error);
    req.flash("errors", error);
    return res.render("admin/user/addUser.ejs", {
      user: user,
      infos: req.flash("infos"),
      errors: req.flash("errors"),
      partners: partners,
    });
  }
};
let showUser = async (req, res) => {
  let user = req.user;
  return res.render("admin/user/showUser.ejs", { user: user });
};
let deleteUser = async (req, res) => {
  try {
    let id = req.params.id;
    await userService.deleteUser(id);
    req.flash("infos", "Utilisateur supprimé avec succès");
  } catch (e) {
    console.log(e);
    req.flash("errors", e);
  }
  return res.redirect("/admin/users");
};

// Investment
let getInvestment = (req, res) => {
  let user = req.user;
  return res.render("admin/investment/listInvestment.ejs", { user: user });
};
let getInvestmentList = async (req, res) => {
  let user = req.user;
  let demands = [];

  if (user.isInvestor()) {
    demands = await demandService.getInvestorDemands(user.id);
    return res.render("admin/investment/listInvestment.ejs", { user: user,
      infos: req.flash("infos"),
      errors: req.flash("errors"),
      demands: demands, });
  } else {
    return res.redirect("/admin/demands");
  }
  
};
let addInvestment = async (req, res) => {
  let user = req.user;
  let partners = await partnerService.getAll();
  let plans = await investmentPlanService.getAll();
  return res.render("admin/investment/addInvestment.ejs", {
    user: user,
    partners,
    infos: req.flash("infos"),
    errors: req.flash("errors"),
    plans,
  });
};
let saveInvestment = async (req, res) => {
  try {
    let user = req.user;
    let idFile;
    let secondaryFile;
    let files = [];

    console.log('BODY>>>>', req.body)
    console.log('FILES>>>>', !req.files['idFile'])

    if (!req.files['idFile']) {
      if (!req.files['secondaryFile']) 
        req.flash("errors", "Aucun fichier n'a été uploadé.");
      else 
        req.flash("errors", "Le fichier KYC doit etre uploadé.");
      let partners = await partnerService.getAll();
      let plans = await investmentPlanService.getAll();
      return res.render("admin/investment/addInvestment.ejs", {
        user: user,
        partners,
        infos: req.flash("infos"),
        errors: req.flash("errors"),
        plans,
      });
    } else {
      idFile = await attachmentService.createNewAttachment({
        url: req.files.idFile[0].filename,
        createdAt: Date.now(),
        updatedAt: Date.now(),
      })
      files.push(idFile)
      if (req.files.secondaryFile) {
        secondaryFile = await attachmentService.createNewAttachment(
          {
            url: req.files.secondaryFile[0].filename,
            createdAt: Date.now(),
            updatedAt: Date.now(),
          }
        )
        files.push(secondaryFile)
      }
      let demandForm = {
        userId: user.id,
        investmentPlanId: req.body.investmentPlanId,
        partnerId: req.body.partnerId,
        amount: req.body.amount,
        createdAt: Date.now(),
        updatedAt: Date.now(),
      };
      let demand = await demandService.createNewDemand(demandForm);
      console.log('DEMAND', demand)
      console.log('FILES', files)
  
      await demand.setAttachments(files)
  
      req.flash("infos", "Votre demande a été enregistré avec succès.");
      return res.redirect("/admin");
    }
    
    
  } catch (err) {
    console.log(err);
    req.flash("errors", err);
  }
};
let showInvestment = async (req, res) => {
  try {
    let user = req.user;
    let demandId = req.params.id;
    let demand = await demandService.getDemand(demandId);
    return res.render("admin/investment/showInvestment.ejs", { user: user, demand: demand });
  } catch (err) {
    console.log(err);
    req.flash("errors", err);
    return res.redirect("/admin");
  }
  
};
let getInvestor = (req, res) => {
  let user = req.user;
  return res.render("admin/investment/listInvestor.ejs", { user: user });
};

//
let createNewUser = async (req, res) => {
  let user = req.body;
  let message = await userService.createNewUser(user);
  console.log(message);
  return res.redirect("/");
};

module.exports = {
  createNewUser: createNewUser,
  getAdminDashboard: getAdminDashboard,
  // User
  getUserList: getUserList,
  getUserListByPartner: getUserListByPartner,
  addUser: addUser,
  editUser: addUser,
  deleteUser: deleteUser,
  saveUser: saveUser,
  showUser: showUser,
  // Partner
  getPartner: getPartner,
  getPartnerList: getPartnerList,
  addPartner: addPartner,
  savePartner: savePartner,
  updatePartner: updatePartner,
  editPartner: editPartner,
  deletePartner: deletePartner,
  // Plan
  getPlan: getPlan,
  getPlanList: getPlanList,
  addPlan: addPlan,
  savePlan: savePlan,
  updatePlan: updatePlan,
  editPlan: editPlan,
  deletePlan: deletePlan,
  // Demand
  getDemand: getDemand,
  getDemandList: getDemandList,
  validateDemand: validateDemand,
  // Process
  getProcess: getProcess,
  getProcessList: getProcessList,
  launchProcess: launchProcess,
  updateProcess: updateProcess,
  // Investment
  getInvestor: getInvestor,
  getInvestment: getInvestment,
  getInvestmentList: getInvestmentList,
  addInvestment: addInvestment,
  saveInvestment: saveInvestment,
  showInvestment: showInvestment,
};
