let checkLoggedIn = (req, res, next) => {
  if (!req.isAuthenticated()) {
    return res.redirect("/login");
  }
  next();
};

let checkLoggedOut = (req, res, next) => {
  if (req.isAuthenticated()) {
    return res.redirect("/admin");
  }
  next();
};

let logout = (req, res) => {
  req.session.destroy(function (error) {
    return res.redirect("/");
  });
};

module.exports = {
  checkLoggedIn: checkLoggedIn,
  checkLoggedOut: checkLoggedOut,
  logout: logout,
};
