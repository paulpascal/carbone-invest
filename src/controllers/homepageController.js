const { validationResult } = require("express-validator");
const userService = require("../services/userService");
const { registerValidate } = require("../validation/authValidation");

const moment = require("moment");

let getHomePage = (req, res) => {
  return res.render("front_office/homepage.ejs", { moment: moment });
};

let getInvestPage = (req, res) => {
  return res.render("front_office/investpage.ejs", { moment: moment });
};

let getTradingCampPage = (req, res) => {
  return res.render("front_office/tradeschoolpage.ejs", { moment: moment });
};

let getCompanyDetailPage = (req, res) => {
  return res.render("front_office/companydetailpage.ejs", { moment: moment });
};

let getSignupPage = (req, res) => {
  let form = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: req.body.password,
    confirmPassword: req.body.confirmPassword,
    address: req.body.address,
    phone: req.body.phone,
  };
  return res.render("auth/signuppage.ejs", {
    errors: req.flash("errors"),
    form: form,
  });
};

let getLoginPage = async (req, res) => {
  let form = {
    email: req.body.email,
  };
  return res.render("auth/loginpage.ejs", {
    errors: req.flash("errors"),
    infos: req.flash("infos"),
    form: form,
  });
};

let handleSignup = async (req, res) => {
  // old input data
  let form = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: req.body.password,
    confirmPassword: req.body.confirmPassword,
    address: req.body.address,
    phone: req.body.phone,
  };

  try {
    // validate input fields
    let errorsArr = [];
    let validationError = validationResult(req);

    if (!validationError.isEmpty()) {
      let errors = Object.values(validationError.mapped());
      errors.forEach((item) => {
        errorsArr.push(item.msg);
      });
      req.flash("errors", errorsArr);

      return res.render("auth/signuppage.ejs", {
        errors: req.flash("errors"),
        form: form,
      });
    }

    // create a new user
    let user = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
      confirmPassword: req.body.confirmPassword,
      phone: req.body.phone,
      address: req.body.address,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    };
    await userService.createNewUser(user);
    req.flash(
      "infos",
      "Votre compte a été bien créé avec succès. <br/> Un mail a été envoyé sur l'adresse email renseignée."
    );
    return res.redirect("/login");
  } catch (error) {
    req.flash("errors", error);

    return res.render("auth/signuppage.ejs", {
      errors: req.flash("errors"),
      form: form,
    });
  }
};

module.exports = {
  getHomePage: getHomePage,
  getInvestPage: getInvestPage,
  getTradingCampPage: getTradingCampPage,
  getCompanyDetailPage: getCompanyDetailPage,
  getLoginPage: getLoginPage,
  getSignupPage: getSignupPage,
  handleSignup: handleSignup,
};
