"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Attachment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` Attachment will call this method automatically.
     */
    static associate(models) {
      // define association here
      Attachment.belongsToMany(models.Demand, {
        through: "DemandsAttachment",
        foreignKey: "attachmentId",

      });

    }
  }
  Attachment.init(
    {
      url: DataTypes.STRING,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Attachment",
      paranoid: true,
    }
  );
  return Attachment;
};
