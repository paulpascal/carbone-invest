"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Demand extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Demand.belongsTo(models.User, { foreignKey: "userId" });
      Demand.belongsTo(models.Partner, { foreignKey: "partnerId" });
      Demand.hasMany(models.Process, {
        foreignKey: "demandId",
        onDelete: "CASCADE",
      });
      Demand.belongsToMany(models.Attachment, { through: "DemandsAttachment", foreignKey: 'demandId', as: 'attachments' });
      Demand.belongsTo(models.InvestmentPlan, { foreignKey: "investmentPlanId" });
      Demand.belongsTo(models.Attachment, { foreignKey: "contractFileId", as: 'contract' });
    }
  }
  Demand.init(
    {
      state: DataTypes.BOOLEAN,
      amount: DataTypes.INTEGER,
      userId: DataTypes.INTEGER,
      partnerId: DataTypes.INTEGER,
      investmentPlanId: DataTypes.INTEGER,
      createdAt: DataTypes.DATE,
    },
    {
      sequelize,
      paranoid: true,
      modelName: "Demand",
    }
  );
  return Demand;
};
