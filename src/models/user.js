"use strict";
const { Model } = require("sequelize");
const { ROLES } = require("../config/data");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.belongsTo(models.Partner, {
        onDelete: "CASCADE",
        foreignKey: {
          name: "partnerId",
          allowNull: true,
        },
      });
    }

    isAdmin() {
      return this.role == ROLES.admin;
    }
    isSuperAdmin() {
      return this.role == ROLES.superAdmin;
    }
    isPartner() {
      return this.role == ROLES.partner;
    }
    isInvestor() {
      return this.role == ROLES.investor;
    }
  }
  User.init(
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      address: DataTypes.STRING,
      role: DataTypes.STRING,
      partnerId: DataTypes.INTEGER,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
      deletedAt: { type: DataTypes.DATE, defaultValue: null },
    },
    {
      sequelize,
      paranoid: true,
      modelName: "User",
    }
  );
  return User;
};
