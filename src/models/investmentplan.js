'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class InvestmentPlan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      InvestmentPlan.belongsTo(models.Partner, { foreignKey: "partnerId" });
    }
  };
  InvestmentPlan.init({
    title: DataTypes.STRING,
    minAmount: DataTypes.INTEGER,
    maxAmount: DataTypes.INTEGER,
    partnerId: DataTypes.INTEGER,
    startAt: DataTypes.DATE,
    endAt: DataTypes.DATE,
    createdAt: DataTypes.DATE,
    
  }, {
    sequelize,
    modelName: 'InvestmentPlan',
  });
  return InvestmentPlan;
};