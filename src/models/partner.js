"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Partner extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Partner.hasMany(models.User, {
        foreignKey: "partnerId",
      });
      Partner.belongsTo(models.PartnerType, {
        foreignKey: "partnerTypeId",
      });
    }
  }
  Partner.init(
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      address: DataTypes.STRING,
      description: DataTypes.STRING,
      phone: DataTypes.STRING,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Partner",
    }
  );
  return Partner;
};
