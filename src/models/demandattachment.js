"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class DemandsAttachment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` DemandsAttachment will call this method automatically.
     */
    static associate(models) {
      // define association here
      DemandsAttachment.belongsTo(models.Demand, {
        foreignKey: "demandId",
      });
      DemandsAttachment.belongsTo(models.Attachment, {
        foreignKey: "attachmentId",
      });
    }
  }
  DemandsAttachment.init(
    {
      demandId: DataTypes.INTEGER,
      attachmentId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "DemandsAttachment",
      paranoid: true,
    }
  );
  return DemandsAttachment;
};
