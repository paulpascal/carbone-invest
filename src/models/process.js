"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Process extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Process.belongsTo(models.Demand, {
        foreignKey: "demandId",
      });
    }
  }
  Process.init(
    {
      demandId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Process",
      paranoid: true,
    }
  );
  return Process;
};
