"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    return queryInterface.bulkInsert(
      "PartnerTypes",
      [
        {
          title: "Investissement en bourse",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Investissement dans l'innovation",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Investissement dans le social",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Investissement dans les actions en entreprise",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
