const db = require("../models");

let createNewPlan = (plan) => {
  return new Promise(async (resolve, reject) => {
    try {
      
      //create plan
      await db.InvestmentPlan.create(plan);
      resolve("Opération effectuée avec succès.");
      
    } catch (e) {
      reject(e);
    }
  });
};

let updatePlan = (id, planData) => {
  return new Promise(async (resolve, reject) => {
    try {
      let plan = await db.InvestmentPlan.findByPk(id);
      if (plan != null) {
        await plan.update(planData);
        resolve("Opération effectuée avec succès.");
      } else {
        reject(`Ce plan n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let getPlan = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let plan = await db.InvestmentPlan.findByPk(id, { include: db.Patner });
      if (plan != null) {
        resolve(plan);
      } else {
        reject(`Ce plan n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let deletePlan = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let plan = await db.InvestmentPlan.findByPk(id);
      if (plan != null) {
        await plan.destroy();
        resolve("Opération effectuée avec succès.");
      } else {
        reject(`Ce plan n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let getAll = (partnerId = null) => {
  return new Promise(async (resolve, reject) => {
    try {
      let plans;
      if (partnerId == null)
        plans = await db.InvestmentPlan.findAll({ include: db.Partner });
      else 
        plans = await db.InvestmentPlan.findAll({
          where: {
            partnerId: partnerId,
          },
          include: db.Partner
        });

      if (plans) resolve(plans);
      resolve(false);
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = {
  createNewPlan: createNewPlan,
  updatePlan: updatePlan,
  deletePlan: deletePlan,
  getAll: getAll,
  getPlan: getPlan,
};
