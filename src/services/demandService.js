const db = require("../models");

let createNewDemand = (demandData) => {
  return new Promise(async (resolve, reject) => {
    try {
    
        //create demand
        let demand = await db.Demand.create(demandData);
        resolve(demand);
      
    } catch (e) {
      reject(e);
    }
  });
};

let updateDemand = (id, demandData) => {
  return new Promise(async (resolve, reject) => {
    try {
      let demand = await db.Demand.findByPk(id);
      if (demand != null) {
        await demand.update(demandData);
        resolve(demand);
      } else {
        reject(`Cette demande n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let getDemand = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let demand = await db.Demand.findByPk(id, {
        include: [
          { all: true },
        ]
      });
      if (demand != null) {
        resolve(demand);
      } else {
        reject(`Cette demande n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let deleteDemand = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let demand = await db.Demand.findByPk(id);
      if (demand != null) {
        await demand.destroy();
        resolve("Opération effectuée avec succès.");
      } else {
        reject(`Cette demande n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let getAll = (state = null) => {
  return new Promise(async (resolve, reject) => {
    try {
      let demands;
      console.log('ADADADD:', state)
      if (state == null) {
        demands = await db.Demand.findAll({ include: db.User }, );
      } else {
        demands = await db.Demand.findAll({ include: db.User, 
          where: {
            state: state
          }
        }, );
      }
      if (demands) resolve(demands);
      resolve(false);
    } catch (e) {
        console.error(e)
    //   reject(e);
    }
  });
};

let getPartnerDemands = (partnerId, state = null) => {
  return new Promise(async (resolve, reject) => {
    try {
      let demands;
      if (state == null) {
        demands = await db.Demand.findAll({
          where: {
            partnerId: partnerId
          }
        }, { include: db.DemandType, include: db.User });
      } else {
        demands = await db.Demand.findAll({
          where: {
            partnerId: partnerId,
            state: state
          }
        }, { include: db.DemandType, include: db.User });
      }
      
      
      if (demands) resolve(demands);
      resolve(false);
    } catch (e) {
        console.error(e)
    //   reject(e);
    }
  });
};

let getInvestorDemands = (userId, state = null) => {
  return new Promise(async (resolve, reject) => {
    try {
      let demands;
      if (state == null) {
        demands = await db.Demand.findAll({
          where: {
            userId: userId
          }, include: [
            { all: true },
          ] });
      } else {
        demands = await db.Demand.findAll({
          where: {
            userId: userId,
            state: state
          }, include: [
            { all: true },
          ] });
      }
      
      if (demands) resolve(demands);
      resolve(false);
    } catch (e) {
        console.error(e)
    //   reject(e);
    }
  });
};

module.exports = {
  createNewDemand: createNewDemand,
  updateDemand: updateDemand,
  deleteDemand: deleteDemand,
  getAll: getAll,
  getPartnerDemands: getPartnerDemands,
  getInvestorDemands: getInvestorDemands,
  getDemand: getDemand,
};
