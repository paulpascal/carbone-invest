const bcrypt = require("bcryptjs");
const db = require("../models");

let findUserByEmail = (emailInput) => {
  return new Promise(async (resolve, reject) => {
    try {
      let user = await db.User.findOne({
        where: {
          email: emailInput,
        },
      });

      console.log(">>>>> PASSPORT USER FOUND findUserByEmail: ", user);

      if (user != null) resolve(user);
      else
        reject(`Aucun compte associé à cette adresse email: '${emailInput}'.`);
    } catch (error) {
      console.log(">>>>> PASSPORT USER FOUND findUserByEmail: ", error);
      reject(error);
    }
  });
};

let findUserById = (idInput) => {
  return new Promise(async (resolve, reject) => {
    try {
      let user = await db.User.findOne({
        where: {
          id: idInput,
        },
      });

      if (user != null) resolve(user);
      else reject("Cet utilisateur est introuvable.");
    } catch (error) {
      reject(error);
    }
  });
};

let comparePassword = (password, userObj) => {
  return new Promise(async (resolve, reject) => {
    try {
      let isMatch = await bcrypt.compare(password, userObj.password);
      if (isMatch) resolve(true);
      else reject("Mot de passe incorrect");
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  findUserById: findUserById,
  findUserByEmail: findUserByEmail,
  comparePassword: comparePassword,
};
