const db = require("../models");

let createNewAttachment = (attachmentData) => {
  return new Promise(async (resolve, reject) => {
    try {
    
        //create attachment
        let attachment = await db.Attachment.create(attachmentData);
        resolve(attachment);
      
    } catch (e) {
      reject(e);
    }
  });
};


let getAttachment = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let attachment = await db.Attachment.findByPk(id);
      if (attachment != null) {
        resolve(attachment);
      } else {
        reject(`Ce fichier n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let deleteAttachment = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let attachment = await db.Attachment.findByPk(id);
      if (attachment != null) {
        await attachment.destroy();
        resolve("Opération effectuée avec succès.");
      } else {
        reject(`Ce fichier n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let getAll = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let attachments = await db.DemandAttachment.findAll();
      
      if (attachments) resolve(attachments);
      resolve(false);
    } catch (e) {
        console.error(e)
    //   reject(e);
    }
  });
};


module.exports = {
  createNewAttachment: createNewAttachment,
  deleteAttachment: deleteAttachment,
  getAll: getAll,
  getAttachment: getAttachment,
};
