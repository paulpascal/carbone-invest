const db = require("../models");

let createNewPartner = (partner) => {
  return new Promise(async (resolve, reject) => {
    try {
      // check email exists before
      let doesEmailExist = await checkPartnerEmailExist(partner);
      if (doesEmailExist) {
        reject(`Cette adresse email ${partner.email} est déjà utilisée.`);
      } else {
        //create partner
        await db.Partner.create(partner);
        resolve("Opération effectuée avec succès.");
      }
    } catch (e) {
      reject(e);
    }
  });
};

let updatePartner = (id, partnerData) => {
  return new Promise(async (resolve, reject) => {
    try {
      let partner = await db.Partner.findByPk(id);
      if (partner != null) {
        await partner.update(partnerData);
        resolve("Opération effectuée avec succès.");
      } else {
        reject(`Ce partenaire n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let getPartner = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let partner = await db.Partner.findByPk(id, { include: db.PartnerType });
      if (partner != null) {
        resolve(partner);
      } else {
        reject(`Ce partenaire n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let deletePartner = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let partner = await db.Partner.findByPk(id);
      if (partner != null) {
        await partner.destroy();
        resolve("Opération effectuée avec succès.");
      } else {
        reject(`Ce partenaire n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let checkPartnerEmailExist = (partnerToCheck) => {
  return new Promise(async (resolve, reject) => {
    try {
      let currentPartner = await db.Partner.findOne({
        where: {
          email: partnerToCheck.email,
        },
      });

      if (currentPartner) resolve(true);
      resolve(false);
    } catch (e) {
      reject(e);
    }
  });
};

let getAll = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let partners = await db.Partner.findAll({ include: db.PartnerType });

      if (partners) resolve(partners);
      resolve(false);
    } catch (e) {
      reject(e);
    }
  });
};

let getAllTypes = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let types = await db.PartnerType.findAll();

      if (types) resolve(types);
      resolve(false);
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = {
  createNewPartner: createNewPartner,
  updatePartner: updatePartner,
  deletePartner: deletePartner,
  getAll: getAll,
  getPartner: getPartner,
  getAllTypes: getAllTypes,
};
