const bcrypt = require("bcryptjs");
const db = require("../models");
const nodemailer = require("nodemailer");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
var ejs = require("ejs");

let createNewUser = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      // check email exists before
      let doesEmailExist = await checkUserEmailExist(user);
      if (doesEmailExist) {
        reject(`Cette adresse email ${user.email} est déjà utilisée.`);
      } else {
        // hash password
        let salt = await bcrypt.genSaltSync(10);
        let plainPassword = user.password;
        // update user password
        user.password = await bcrypt.hashSync(user.password, salt);
        let createdUser = await db.User.create(user);

        //create user
        try {
          if (createdUser.isInvestor()) {
            var transporter = nodemailer.createTransport({
              service: "gmail",
              host: "smtp.gmail.com",
              secureConnection: true,
              auth: {
                user: "samuelgnagn@gmail.com",
                pass: "cggrtxptcpzlohlb",
              },
            });
  
            ejs.renderFile(
              __dirname + "/../views/admin/user/email.ejs",
              { user: user, password: plainPassword },
              function (err, data) {
                if (err) {
                  console.log(err);
                } else {
                  var mainOptions = {
                    from: "samuelgnagn@gmail.com",
                    to: user.email,
                    subject: "Compte activé",
                    html: data,
                  };
                  //console.log("html data ======================>", mainOptions.html);
  
                  transporter.sendMail(mainOptions, function (err, info) {
                    if (err) {
                      res.json({
                        msg: "fail",
                      });
                    } else {
                      res.json({
                        msg: "success",
                      });
                    }
                  });
                }
              }
            );
          }
        } catch (err) {
          console.log('MAIL NOT SENT: MAYBE OFFLINE')
        }
        // send mail
        resolve("Opération effectuée avec succès.");
      }
    } catch (e) {
      reject(e);
    }
  });
};

let updateUser = (id, userData) => {
  return new Promise(async (resolve, reject) => {
    try {
      let user = await db.User.findByPk(id);
      if (user != null) {
        await user.update(userData);
        resolve("Opération effectuée avec succès.");
      } else {
        reject(`Ce utilisateur n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let getUser = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let user = await db.User.findByPk(id);
      if (user != null) {
        resolve(user);
      } else {
        reject(`Ce utilisateur n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let getUsers = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let users = await db.User.findAll({
        where: {
          role: {
            [Op.ne]: "ROLE_SUPERADMIN",
          },
        },
      });
      resolve(users);
    } catch (e) {
      reject(e);
    }
  });
};

let deleteUser = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let user = await db.User.findByPk(id);
      if (user != null) {
        await user.destroy();
        resolve("Opération effectuée avec succès.");
      } else {
        reject(`Ce utilisateur n'a pas été trouvé.`);
      }
    } catch (e) {
      reject(e);
    }
  });
};

let checkUserEmailExist = (userToCheck) => {
  return new Promise(async (resolve, reject) => {
    try {
      let currentUser = await db.User.findOne({
        where: {
          email: userToCheck.email,
        },
      });

      if (currentUser) resolve(true);
      resolve(false);
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = {
  createNewUser: createNewUser,
  updateUser: updateUser,
  deleteUser: deleteUser,
  getUser: getUser,
  getUsers: getUsers,
};
