const { check } = require("express-validator");

let validateRegiser = [
  check("email", "Adresse email invalide").isEmail().trim(),
  check("password", "Mot de passe invalide, minimum 6 caractères").isLength({
    min: 6,
  }),
  check("confirmPassword", "Les mots de passe ne correspondent pas.").custom(
    (value, { req }) => {
      return value == req.body.password;
    }
  ),
];

module.exports = {
  validateRegiser: validateRegiser,
};
