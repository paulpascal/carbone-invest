"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("DemandsAttachments", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      demandId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "Demands",
          key: "id",
        },
      },
      attachmentId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "Attachments",
          key: "id",
        },
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("DemandsAttachments");
  },
};
